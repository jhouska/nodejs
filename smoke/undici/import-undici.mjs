// SPDX-FileCopyrightText: Red Hat
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import {strict as assert} from 'node:assert';
import undici from 'node:internal/deps/undici/undici';

const HTTPBIN_HOSTNAME = process.env.HTTPBIN_HOSTNAME || 'localhost';

const response = await undici.fetch(`http://${HTTPBIN_HOSTNAME}/user-agent`);

assert.equal(response.status, 200, 'HTTP request was not successful');
assert.deepEqual(await response.json(), {'user-agent': 'node'}, 'Unexpected User-Agent');
