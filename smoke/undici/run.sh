#!/bin/sh -e

# SPDX-FileCopyrightText: Red Hat
#
# SPDX-License-Identifier: AGPL-3.0-or-later

# run local httpbin process for undici to call

export HTTPBIN_HOSTNAME='127.0.0.1:8080'

# === Setup ===

kill_gunicorn() {
  xargs -- kill --signal TERM --timeout 3000 KILL <gunicorn.pid && rm -f gunicorn.pid
}

cat </dev/null >gunicorn.log
gunicorn --daemon \
  --bind "${HTTPBIN_HOSTNAME}" \
  --log-file gunicorn.log \
  --pid gunicorn.pid \
  httpbin:app
trap kill_gunicorn EXIT

# === Test ===
echo "========================="
${NODEJS_BIN:-node} -e 'console.log("Running test stript run.sh.\nVersion of nodejs:\t", process.version, "\n");'
${NODEJS_BIN:-node} --expose-internals ./import-undici.mjs
echo "========================="
